﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Calculon.Core;
using Calculon.Core.Service;
using Calculon.Core.Resolving;

namespace Calculon.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator calculator = new Calculator();

            System.Console.WriteLine("Ctrl+C for exit\nExpression:");
            string input = System.Console.ReadLine();
            while (input != "\\c")
            {
                double result = calculator.Calculate(input);

                System.Console.WriteLine(result);
                System.Console.WriteLine("Expression > ");
                input = System.Console.ReadLine();
            }
        }
    }
}
