﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculon.Core.Service;
using System.Collections.Generic;

namespace Calculon.Core.UnitTest
{
    [TestClass]
    public class PostfixCalculationServiceTest
    {
        [TestMethod]
        public void TestCalculate()
        {
            string[] expression = new string[] { "2", "33,3", "2", "/", "+", "44", "2", "^", "-" };
            int expected = -1917;
            var calculationService = new PostfixCalculationService(TestingDataStorage.GetResolveConfig());

            int actual = (int)calculationService.Calculate(expression);

            Assert.AreEqual(expected, actual);
        }
    }
}
