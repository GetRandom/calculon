﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculon.Core.Service;
using System.Collections.Generic;

namespace Calculon.Core.UnitTest
{
    [TestClass]
    public class PostfixNotationConverterTest
    {
        [TestMethod]
        public void TestIsOperand()
        {
            string[] operands = new string[] { "1", "1,0", "1,2" };
            var postfixNotationConverter = new PostfixNotationConverterTestDistributor(TestingDataStorage.GetPriorityConfig());
            foreach (var item in operands)
            {
                Assert.IsTrue(postfixNotationConverter.IsOperand(item));
            }
        }

        [TestMethod]
        public void TestSeparate()
        {
            string expression = @"(2+33,3)/2 - 44^2";
            string[] expected = new string[] { "(", "2", "+", "33,3", ")", "/", "2", "-", "44", "^", "2" };
            var postfixNotationConverter = new PostfixNotationConverterTestDistributor(TestingDataStorage.GetPriorityConfig());

            string[] actual = postfixNotationConverter.Separate(expression);

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestConvertToPosfix()
        {
            string[] expression = new string[] { "2", "+", "33,3", "/", "2", "-", "44", "^", "2" };
            string[] expected = new string[] { "2", "33,3", "2", "/", "+", "44", "2", "^", "-" };
            var postfixNotationConverter = new PostfixNotationConverterTestDistributor(TestingDataStorage.GetPriorityConfig());

            string[] actual = postfixNotationConverter.ConvertToPostfix(expression);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
