﻿using Calculon.Core.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculon.Core.UnitTest
{
    public class PostfixNotationConverterTestDistributor : PostfixNotationConverter
    {
        public PostfixNotationConverterTestDistributor(Dictionary<string, IPrioritized> operations)
            : base(operations) { }

        public new string[] ConvertToPostfix(string[] expression)
        {
            return base.ConvertToPostfix(expression);
        }

        public new string[] Separate(string expression)
        {
            return base.Separate(expression);
        }

        public new bool IsOperand(string token)
        {
            return base.IsOperand(token);
        }
    }
}
