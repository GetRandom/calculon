﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Calculon.Core.Resolving;

namespace Calculon.Core.UnitTest
{
    public static class TestingDataStorage
    {
        public static Dictionary<string, IPrioritized> GetPriorityConfig()
        {
            Dictionary<string, IPrioritized> operations = new Dictionary<string, IPrioritized>();
            operations.Add("-", new Operator(null, 1));
            operations.Add("+", new Operator(null, 1));
            operations.Add("*", new Operator(null, 2));
            operations.Add("/", new Operator(null, 2));
            operations.Add("^", new Operator(null, 3));

            return operations;
        }

        public static Dictionary<string, IResolving> GetResolveConfig()
        {
            Dictionary<string, IResolving> operations = new Dictionary<string, IResolving>();
            operations.Add("-", new Operator(new Subtraction(), 0));
            operations.Add("+", new Operator(new Summation(), 0));
            operations.Add("*", new Operator(new Multiplication(), 0));
            operations.Add("/", new Operator(new Division(), 0));
            operations.Add("^", new Operator(new Power(), 0));

            return operations;
        }
    }
}
