﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculon.Core
{
    public interface ICalculationService
    {
        double Calculate(string[] postfixExpression);
    }
}
