﻿
namespace Calculon.Core
{
    public interface IPrioritized
    {
        int Priority { get; }
    }
}
