﻿
namespace Calculon.Core
{
    public interface IResolving
    {
        double Resolve(params double[] paramaters);
    }
}
