﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculon.Core
{
    public class Operator : IResolving, IPrioritized
    {
        protected IResolving resolver;
        public int Priority { get; private set; }

        public Operator(IResolving resolver, int priority)
        {
            this.resolver = resolver;
            this.Priority = priority;
        }

        public double Resolve(params double[] parameters)
        {
            return this.resolver.Resolve(parameters);
        }
    }
}
