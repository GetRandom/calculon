﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculon.Core.Resolving
{
    public class Multiplication : IResolving
    {
        public double Resolve(params double[] paramaters)
        {
            return paramaters[0] * paramaters[1];
        }
    }
}
