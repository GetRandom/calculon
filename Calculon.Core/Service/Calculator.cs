﻿using Calculon.Core.Resolving;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Calculon.Core.Service
{
    // Фасад для работы с сервисами PostfixNotationConverter и CalculationService
    /// <summary>
    /// Сервис для вычисления выражений
    /// </summary>
    public class Calculator : ICalculator
    {
        private IPostfixNotationConverter postfixNotationConverter;
        private ICalculationService calculationService;

        public Calculator()
        {
            Dictionary<string, Operator> config = new Dictionary<string, Operator>();
            config.Add("-", new Operator(new Subtraction(), 1));
            config.Add("+", new Operator(new Summation(), 1));
            config.Add("*", new Operator(new Multiplication(), 2));
            config.Add("/", new Operator(new Division(), 2));
            config.Add("^", new Operator(new Power(), 3));
            Dictionary<string, IPrioritized> prioritized = new Dictionary<string, IPrioritized>();
            Dictionary<string, IResolving> resolving = new Dictionary<string, IResolving>();
            foreach (var pair in config)
            {
                prioritized.Add(pair.Key, pair.Value);
                resolving.Add(pair.Key, pair.Value);
            }
            this.postfixNotationConverter = new PostfixNotationConverter(prioritized);
            this.calculationService = new PostfixCalculationService(resolving);
        }

        /// <summary>
        /// Вычисляет выражение в строке
        /// </summary>
        /// <returns>Результат вычисления</returns>
        public double Calculate(string expression)
        {
            string[] postfix = this.postfixNotationConverter.ConvertToPostfix(expression);
            double result = this.calculationService.Calculate(postfix);
            return result;
        }
    }
}
