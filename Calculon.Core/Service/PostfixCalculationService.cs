﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Calculon.Core.Service
{
    public class PostfixCalculationService : ICalculationService
    {
        protected Dictionary<string, IResolving> operations;

        public PostfixCalculationService(Dictionary<string, IResolving> operations)
        {
            this.operations = operations;
        }

        /// <summary>
        /// Вычисляет выражения в ОПН
        /// </summary>
        /// <param name="postfixExpression">выражение в ОПН</param>
        /// <returns>Результат вычисления</returns>
        public double Calculate(string[] postfixExpression)
        {
            Stack<double> stack = new Stack<double>();
            for (int i = 0; i < postfixExpression.Length; i++)
            {
                if (operations.ContainsKey(postfixExpression[i]))
                {
                    double right = stack.Pop();
                    double left = stack.Pop();
                    double result = this.operations[postfixExpression[i]].Resolve(left, right);
                    stack.Push(result);
                }
                else
                {
                    stack.Push(Double.Parse(postfixExpression[i]));
                }
            }
            return stack.Pop();
        }
    }
}
