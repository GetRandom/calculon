﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Calculon.Core.Service
{
    public class PostfixNotationConverter : IPostfixNotationConverter
    {
        protected Dictionary<string, IPrioritized> operations;

        public PostfixNotationConverter(Dictionary<string, IPrioritized> operations)
        {
            this.operations = operations;
        }

        /// <summary>
        /// Преобразует строку в ОПН выражение
        /// </summary>
        /// <param name="expression">Строка с выражением для парсинга</param>
        /// <returns>Массив элементов выражения (операнды, операторы)</returns>
        public string[] ConvertToPostfix(string expression)
        {
            string[] postfixExpression = this.Separate(expression);
            return this.ConvertToPostfix(postfixExpression);
        }

        /// <param name="expression">Массив из операндов и операторов</param>
        /// <returns>Массив упорядоченный в ОПН</returns>
        protected string[] ConvertToPostfix(string[] expression)
        {
            Queue<string> qExpression = new Queue<string>(expression);
            Queue<string> postfixExpression = new Queue<string>();
            Stack<string> operators = new Stack<string>();
            while (qExpression.Count > 0)
            {
                string token = qExpression.Dequeue();
                if (this.IsOperand(token))
                    postfixExpression.Enqueue(token);
                else
                {
                    if (operators.Count > 0 && this.operations[token].Priority > this.operations[operators.Peek()].Priority)
                    {
                        postfixExpression.Enqueue(qExpression.Dequeue());
                        postfixExpression.Enqueue(token);
                    }
                    else
                    {
                        if (operators.Count > 0)
                            postfixExpression.Enqueue(operators.Pop());
                        operators.Push(token);
                    }
                }
            }
            postfixExpression.Enqueue(operators.Pop());
            return postfixExpression.ToArray();
        }

        /// <summary>
        /// Разбивает строку на составляющие выржаения
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        protected string[] Separate(string expression)
        {
            Func<string, bool> notEmptyPredicate = (string str) => { return !string.IsNullOrWhiteSpace(str); };
            string pattern = @"([\^\*\+/\-/(/)])";
            string[] splittedExpression = Regex.Split(expression.Replace(" ", ""), pattern).Where(notEmptyPredicate).ToArray();
            return splittedExpression;
        }

        protected bool IsOperand(string token)
        {
            return Regex.IsMatch(token, @"[0-9]");
        }
    }
}
